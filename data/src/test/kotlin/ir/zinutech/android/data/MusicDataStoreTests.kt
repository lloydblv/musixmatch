package ir.zinutech.android.data

import io.reactivex.Single
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.api.ChartTracksResponse
import ir.zinutech.android.data.api.ChartTracksResponse.Body
import ir.zinutech.android.data.api.ChartTracksResponse.TrackDataWrapper
import ir.zinutech.android.data.api.LyricsDetailResponse
import ir.zinutech.android.data.repositories.MusicDataStore
import ir.zinutech.android.data.utils.DataTestUtils
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito


class MusicDataStoreTests {

  private lateinit var api: Api
  private lateinit var musicDataStore: MusicDataStore

  @Before
  fun before() {
    api = Mockito.mock(Api::class.java)
    musicDataStore = MusicDataStore(api)
  }

  @Test
  fun testWhenRequestingTopTracksFromRemoteReturnExpectedResult() {
    val chartTracksResponse = ChartTracksResponse(DataTestUtils.generateSuccessfulHeader(),
        Body(DataTestUtils.generateTopTracksDataList().map {
          TrackDataWrapper(it)
        }))

    Mockito.`when`(api.getChartTracksList(pageIndex = 0)).thenReturn(
        Single.just(chartTracksResponse))

    musicDataStore.getTracksList(pageIndex = 0).test()
        .assertValue { it.size == 6 && it[0].artistName == "artistName0" }
        .assertComplete()
  }

  @Test
  fun testWhenRequestingLyricsFromRemoteReturnExpectedResult() {

    val lyricsDetailResponse = LyricsDetailResponse(DataTestUtils.generateSuccessfulHeader(),
        LyricsDetailResponse.Body(DataTestUtils.getTestLyricsData()))

    Mockito.`when`(api.getLyrics("1")).thenReturn(Single.just(lyricsDetailResponse))

    musicDataStore.getLyricsByTrackId("1").map { it.value }.test()
        .assertValue { it.lyricsList.size == 6 && it.lyricsList[0] == "Lyrics0" }
        .assertComplete()
  }

}