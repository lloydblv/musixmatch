package ir.zinutech.android.data.mappers

import ir.zinutech.android.data.entities.TrackData
import ir.zinutech.android.domain.common.Mapper
import ir.zinutech.android.domain.entities.TrackEntity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TrackListDataEntityMapper @Inject constructor() : Mapper<List<TrackData>, List<TrackEntity>>() {
  override fun mapFrom(from: List<TrackData>): List<TrackEntity> {
    return from.map {
      val smallAlbumArt = it.album_coverart_100x100
      val normalAlbumArt = it.album_coverart_350x350.takeIf { it.isNotEmpty() }
          ?: smallAlbumArt
      val mediumAlbumArt = it.album_coverart_500x500.takeIf { it.isNotEmpty() }
          ?: normalAlbumArt
      TrackEntity(
          it.track_id,
          it.track_name ?: "${it.track_id}",
          it.album_name,
          it.artist_name,
          smallAlbumArt,
          normalAlbumArt,
          mediumAlbumArt,
          it.album_coverart_800x800.takeIf { it.isNotEmpty() } ?: mediumAlbumArt
      )
    }
  }
}