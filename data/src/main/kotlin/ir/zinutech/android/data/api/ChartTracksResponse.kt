package ir.zinutech.android.data.api

import ir.zinutech.android.data.api.BaseApiResponse.Header
import ir.zinutech.android.data.entities.TrackData

data class ChartTracksResponse(override val header: Header,
    val body: Body) : BaseApiResponse {
  data class Body(val track_list: List<TrackDataWrapper>)
  data class TrackDataWrapper(val track: TrackData)
}