package ir.zinutech.android.data.api

import ir.zinutech.android.data.api.BaseApiResponse.Header
import ir.zinutech.android.data.entities.LyricsData

data class LyricsDetailResponse(override val header: Header,
    val body: Body) : BaseApiResponse {
  data class Body(val lyrics: LyricsData)
}