package ir.zinutech.android.data.api.converters

/**
 * Created by mohammadrezanajafi on 12/25/17.
 */

import java.lang.annotation.Retention

import java.lang.annotation.ElementType.METHOD
import java.lang.annotation.RetentionPolicy.RUNTIME

/**
 * An annotation for identifying the payload that we want to extract from an API response wrapped in
 * an envelope object.
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
//@Retention(RUNTIME)
annotation class EnvelopePayload(val value: String = "")
