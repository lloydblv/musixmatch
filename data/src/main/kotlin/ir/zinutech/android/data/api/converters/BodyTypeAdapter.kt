package ir.zinutech.android.data.api.converters

import android.util.Log
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.lang.reflect.Type

class BodyTypeAdapter<T>/* : TypeAdapter<Body<T>>() {
  override fun write(out: JsonWriter?, value: Body<T>) {

  }

  override fun read(`in`: JsonReader?): Body<T> {

    val type: Type = object : TypeToken<T>() {}.type
    val adapter = Gson().getAdapter(TypeToken.get(type ))

    Log.e("BodyTypeAdapter", "type:$type, adapter:$adapter")


    `in`?.beginObject()
    `in`?.nextName()
    val result = Body(adapter.read(`in`) as T)
    return result
  }
}*/