package ir.zinutech.android.data.repositories

import io.reactivex.Single
import ir.zinutech.android.domain.MusicRepository
import ir.zinutech.android.domain.entities.LyricsEntity
import ir.zinutech.android.domain.entities.Optional
import ir.zinutech.android.domain.entities.TrackEntity

class MusicRepositoryImpl(private val musicDataStore: MusicDataStore) : MusicRepository {

  override fun getTracksList(
      pageIndex: Int): Single<List<TrackEntity>> = musicDataStore.getTracksList(pageIndex)


  override fun getLyrics(
      trackId: String): Single<Optional<LyricsEntity>> = musicDataStore.getLyricsByTrackId(
      trackId)
}