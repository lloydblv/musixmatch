package ir.zinutech.android.data.entities

data class TrackData(
    val track_id: Long,
    val track_name: String?,
    val album_name: String,
    val artist_name: String,

    val album_coverart_100x100: String,
    val album_coverart_350x350: String,
    val album_coverart_500x500: String,
    val album_coverart_800x800: String)