package ir.zinutech.android.data.api

import io.reactivex.Single
import ir.zinutech.android.data.api.converters.EnvelopePayload
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {


  @GET("chart.tracks.get?country=us&f_has_lyrics=1")
  @EnvelopePayload("message")
  fun getChartTracksList(
      @Query("page") pageIndex: Int): Single<ChartTracksResponse>

  @GET("track.lyrics.get")
  @EnvelopePayload("message")
  fun getLyrics(
      @Query("track_id") trackId: String): Single<LyricsDetailResponse>

}