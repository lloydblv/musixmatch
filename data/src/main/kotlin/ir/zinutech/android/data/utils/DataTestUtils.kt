package ir.zinutech.android.data.utils

import ir.zinutech.android.data.api.BaseApiResponse.Header
import ir.zinutech.android.data.entities.LyricsData
import ir.zinutech.android.data.entities.TrackData
import ir.zinutech.android.domain.common.DomainTestUtils.getTestTrackEntity
import ir.zinutech.android.domain.entities.TrackEntity

object DataTestUtils {
  fun generateSuccessfulHeader(): Header {
    return Header(200, 0.0041)
  }

  fun generateTopTracksDataList(): List<TrackData> = (0..5).map {
    getTestTrackData(it.toLong())
  }

  fun getTestTrackData(id: Long): TrackData =
      TrackData(id, "trackName$id", "albumName$id", "artistName$id",
          "albumArtSmall$id",
          "albumArtNormal$id", "albumArtMedium$id", "albumArtLarge$id")

  fun getTestLyricsData(): LyricsData = LyricsData(generateLyricsBody())

  fun generateLyricsBody(): String{
    val lyricsBuilder = StringBuilder()
    (0..5).map {
      lyricsBuilder
          .append("Lyrics$it")
      if(it != 5){
        lyricsBuilder.append("\n")
      }
    }
    return lyricsBuilder.toString()
  }
}