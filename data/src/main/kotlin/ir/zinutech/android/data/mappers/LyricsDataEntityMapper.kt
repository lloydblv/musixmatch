package ir.zinutech.android.data.mappers

import android.util.Log
import ir.zinutech.android.data.entities.LyricsData
import ir.zinutech.android.domain.common.Mapper
import ir.zinutech.android.domain.entities.LyricsEntity
import ir.zinutech.android.domain.entities.Optional
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LyricsDataEntityMapper @Inject constructor() : Mapper<LyricsData, Optional<LyricsEntity>>() {
  override fun mapFrom(from: LyricsData): Optional<LyricsEntity> {
    return Optional.of(LyricsEntity(from.lyrics_body.split("\n").toList()))
  }
}