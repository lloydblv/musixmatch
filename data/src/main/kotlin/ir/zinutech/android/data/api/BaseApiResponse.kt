package ir.zinutech.android.data.api


interface BaseApiResponse {
  val header: Header
  data class Header(val status_code: Int, val execute_time: Double)
}