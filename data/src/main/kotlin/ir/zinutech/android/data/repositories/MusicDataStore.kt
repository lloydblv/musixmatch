package ir.zinutech.android.data.repositories

import io.reactivex.Single
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.mappers.LyricsDataEntityMapper
import ir.zinutech.android.data.mappers.TrackListDataEntityMapper
import ir.zinutech.android.domain.MusicDataStore
import ir.zinutech.android.domain.entities.LyricsEntity
import ir.zinutech.android.domain.entities.Optional
import ir.zinutech.android.domain.entities.TrackEntity

class MusicDataStore(private val api: Api) : MusicDataStore {

  private val trackListDataEntityMapper = TrackListDataEntityMapper()
  private val lyricsDataEntityMapper = LyricsDataEntityMapper()


  override fun getTracksList(pageIndex: Int): Single<List<TrackEntity>> {
    return api.getChartTracksList(pageIndex).map {
      trackListDataEntityMapper.mapFrom(it.body.track_list.map { it.track })
    }
  }

  override fun getLyricsByTrackId(trackId: String): Single<Optional<LyricsEntity>> {
    return api.getLyrics(trackId).map {
      lyricsDataEntityMapper.mapFrom(it.body.lyrics)
    }
  }
}