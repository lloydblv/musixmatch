package com.company.android.sample.lyrics.domain

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.company.android.sample.lyrics.presenter.LyricsViewModel
import ir.zinutech.android.domain.usecases.GetLyrics

class LyricsVMFactory(
    private val useCase: GetLyrics,
    private val trackId: String
): ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    return LyricsViewModel(useCase, trackId) as T
  }

}