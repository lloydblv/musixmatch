package com.company.android.sample.lyrics.injection

import com.company.android.sample.lyrics.view.LyricsFragment
import dagger.Subcomponent


@Subcomponent(modules = [LyricsModule::class])
interface LyricsSubComponent {
  fun inject(lyricsFragment: LyricsFragment)
}