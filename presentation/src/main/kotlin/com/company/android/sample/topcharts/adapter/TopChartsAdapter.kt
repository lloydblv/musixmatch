package com.company.android.sample.topcharts.adapter

import android.animation.AnimatorInflater
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.view.View
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.company.android.sample.R
import com.company.android.sample.commons.BaseLceAdapter
import com.company.android.sample.commons.BaseViewHolder
import com.company.android.sample.commons.RecyclerViewClickListener
import com.company.android.sample.commons.isL
import com.company.android.sample.commons.isM
import com.company.android.sample.widgets.Divided
import ir.zinutech.android.domain.entities.TrackEntity
import kotlinx.android.synthetic.main.item_loading_layout.view.infinite_loading
import kotlinx.android.synthetic.main.item_topcharts_layout.view.topcharts_item_artist_name_tv
import kotlinx.android.synthetic.main.item_topcharts_layout.view.topcharts_item_cover_iv
import kotlinx.android.synthetic.main.item_topcharts_layout.view.topcharts_item_name_tv
import kotlinx.android.synthetic.main.item_topcharts_layout.view.topcharts_item_parent
import timber.log.Timber

class TopChartsAdapter(private val spanCount: Int,
    private val requestManager: RequestManager,
    onListItemClickListener: RecyclerViewClickListener) : BaseLceAdapter<BaseViewHolder<TrackEntity>, TrackEntity>(
    onListItemClickListener) {

  private var showLoadingMore = false

  companion object {
    private const val CHART_VIEW_TYPE = 11
  }


  init {
    setHasStableIds(true)
  }

  override fun getLayout(viewType: Int): Int = if (viewType == LOADING_VIEW_TYPE) {
    R.layout.item_loading_layout
  } else {
    R.layout.item_topcharts_layout
  }


  override fun getViewHolder(parent: View,
      viewType: Int): BaseViewHolder<TrackEntity> = if (viewType == LOADING_VIEW_TYPE) {
    LoadingViewHolder(parent)
  } else {
    ViewHolder(parent, requestManager)
  }

  override fun onBindViewHolder(holder: BaseViewHolder<TrackEntity>, position: Int) {
    if (getItemViewType(position) == LOADING_VIEW_TYPE) {
      bindLoadingViewHolder(holder as LoadingViewHolder, position)
    } else {
      super.onBindViewHolder(holder, position)
    }
  }

  private fun bindLoadingViewHolder(loadingViewHolder: LoadingViewHolder,
      position: Int) {
    loadingViewHolder.itemView.infinite_loading.visibility =
        if (position > 0 && showLoadingMore) {
          View.VISIBLE
        } else {
          View.INVISIBLE
        }
  }

  override fun getItemViewType(position: Int): Int {
    return if (position < getDataItemCount() && getDataItemCount() > 0) {
      CHART_VIEW_TYPE
    } else {
      LOADING_VIEW_TYPE
    }
  }

  fun getItemColumnSpan(position: Int): Int {
    return when (getItemViewType(position)) {
      LOADING_VIEW_TYPE -> spanCount
      else -> 1
    }
  }

  private fun getItem(position: Int): TrackEntity? {
    return if (position < 0 || position >= mItems.size) null else mItems[position]
  }

  override fun getItemId(position: Int): Long {
    return if (getItemViewType(position) == LOADING_VIEW_TYPE) {
      -1L
    } else getItem(position)!!.trackId
  }

  fun getItemPosition(itemId: Long): Int {
    for (position in mItems.indices) {
      if (getItem(position)!!.trackId == itemId) return position
    }
    return RecyclerView.NO_POSITION
  }

  private fun getLoadingMoreItemPosition(): Int {
    val loadingPos = if (showLoadingMore) itemCount - 1 else RecyclerView.NO_POSITION
    Timber.d("getLoadingMoreItemPosition(), loadingPos:[%s]", loadingPos)
    return loadingPos
  }

  fun dataStartedLoading() {
    if(getDataItemCount() == 0) return
    Timber.d("dataStartedLoading(), showLoadingMore:[%s]", showLoadingMore)
    if (showLoadingMore) return
    showLoadingMore = true
    notifyItemInserted(getLoadingMoreItemPosition())
  }

  fun dataFinishedLoading() {
    Timber.d("dataFinishedLoading(), showLoadingMore:[%s]", showLoadingMore)

    if (!showLoadingMore) return
    val loadingPos = getLoadingMoreItemPosition()
    showLoadingMore = false
    notifyItemRemoved(loadingPos)
  }

  override fun getItemCount(): Int {
    return super.getItemCount() + if(showLoadingMore) 1 else 0
  }

  fun getDataItemCount() = mItems.size
  class ViewHolder(view: View,
      private val requestManager: RequestManager) : BaseViewHolder<TrackEntity>(view), Divided {
    init {
      if (isL()) {
        itemView.topcharts_item_parent.apply {
          stateListAnimator = AnimatorInflater.loadStateListAnimator(context, R.animator.raise)
          ViewCompat.setElevation(this, resources.getDimension(R.dimen.z_card))
        }
      }

      if (isM()) {
        itemView.topcharts_item_name_tv.breakStrategy = Layout.BREAK_STRATEGY_SIMPLE
      }

    }

    override fun bind(item: TrackEntity) {
      itemView.topcharts_item_name_tv.text = item.trackName
      itemView.topcharts_item_artist_name_tv.text = item.artistName
      requestManager
          .load(item.albumArtMedium)
          .apply(RequestOptions()
              .diskCacheStrategy(DiskCacheStrategy.DATA)
              .onlyRetrieveFromCache(false)
              .dontTransform())
          .thumbnail(requestManager.load(item.albumArtSmall))
          .into(itemView.topcharts_item_cover_iv)

      ViewCompat.setTransitionName(itemView.topcharts_item_cover_iv, item.albumArtMedium)
      ViewCompat.setTransitionName(itemView.topcharts_item_name_tv, item.trackName)
    }
  }

  class LoadingViewHolder(parent: View) : BaseViewHolder<TrackEntity>(parent) {
    init {
      itemView.infinite_loading.apply {
        if (isL()) {
//          ViewCompat.tint
//          android:indeterminateTint="@color/mid_grey"
//          android:indeterminateTintMode="src_in"
        }
      }
    }

    override fun bind(item: TrackEntity) {

    }
  }
}