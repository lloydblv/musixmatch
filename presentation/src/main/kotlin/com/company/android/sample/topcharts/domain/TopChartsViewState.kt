package com.company.android.sample.topcharts.domain

import ir.zinutech.android.domain.entities.TrackEntity

data class TopChartsViewState(
    var isLoading: Boolean = true,
    var topCharts: List<TrackEntity>? = null,
    var pageTopCharts: List<TrackEntity>? = null
)