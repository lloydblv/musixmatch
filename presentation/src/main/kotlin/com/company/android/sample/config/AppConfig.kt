package com.company.android.sample.config

import android.app.Application
import com.company.android.sample.BuildConfig
import com.company.android.sample.di.components.DaggerMainComponent
import com.company.android.sample.di.components.MainComponent
import com.company.android.sample.di.modules.ContextModule
import com.company.android.sample.di.modules.DataModule
import com.company.android.sample.di.modules.NetModule
import com.company.android.sample.lyrics.injection.LyricsModule
import com.company.android.sample.lyrics.injection.LyricsSubComponent
import com.company.android.sample.topcharts.injection.TopChartsModule
import com.company.android.sample.topcharts.injection.TopChartsSubComponent
import timber.log.Timber
import timber.log.Timber.DebugTree

class AppConfig : Application() {

  private lateinit var mainComponent: MainComponent
  private var topChartsComponent: TopChartsSubComponent? = null
  private var lyricsComponent: LyricsSubComponent? = null
  override fun onCreate() {
    super.onCreate()

    Timber.plant(DebugTree())

//    if(LeakCanary.isInAnalyzerProcess(this)) return
//    LeakCanary.install(this)

    mainComponent = DaggerMainComponent.builder()
        .contextModule(ContextModule(this))
        .netModule(NetModule(BuildConfig.API_KEY))
        .dataModule(DataModule())
        .build()

  }

  fun createTopChartsComponent(): TopChartsSubComponent {
    topChartsComponent = mainComponent.plus(TopChartsModule())
    return topChartsComponent!!
  }

  fun releaseTopChartsComponent() {
    topChartsComponent = null
  }

  fun createLyricsComponent(): LyricsSubComponent {
    lyricsComponent = mainComponent.plus(LyricsModule())
    return lyricsComponent!!
  }

  fun releaseLyricsComponent() {
    lyricsComponent = null
  }
}