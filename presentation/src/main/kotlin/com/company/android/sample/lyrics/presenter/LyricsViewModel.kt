package com.company.android.sample.lyrics.presenter

import android.arch.lifecycle.MutableLiveData
import com.company.android.sample.lyrics.domain.LyricsViewState
import com.company.android.sample.commons.BaseViewModel
import com.company.android.sample.commons.SingleLiveEvent
import ir.zinutech.android.domain.usecases.GetLyrics
import timber.log.Timber

class LyricsViewModel(private val getLyricsUseCase: GetLyrics,
    private val trackId: String) : BaseViewModel() {

  var viewState: MutableLiveData<LyricsViewState> = MutableLiveData()
  var errorState: SingleLiveEvent<Throwable?> = SingleLiveEvent()

  private var pageIndex: MutableLiveData<Int> = MutableLiveData()

  init {
    viewState.value = LyricsViewState()
    pageIndex.value = 0
  }

  fun getLyrics() {
//    if(true) return
    addDisposable(
        getLyricsUseCase.getByTrackId(trackId)
            .subscribe({ lyric ->
              viewState.value?.let {
                val newState = this.viewState.value?.copy(isLoading = false,
                    lyricsEntity = lyric.value)
                this.viewState.value = newState
                this.errorState.value = null
              }
            }, {
              Timber.e(it, "while getLyrics()")
              viewState.value = viewState.value?.copy(isLoading = false)
              errorState.value = it
            })
    )
  }
}