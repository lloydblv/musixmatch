package com.company.android.sample.lyrics.injection

import com.company.android.sample.commons.AsyncTransformer
import dagger.Module
import dagger.Provides
import ir.zinutech.android.domain.MusicRepository
import ir.zinutech.android.domain.usecases.GetLyrics

@LyricsScope
@Module
class LyricsModule {
  @Provides
  fun provideGetCarModelsUseCase(musicRepository: MusicRepository) = GetLyrics(
      AsyncTransformer(), musicRepository)
}