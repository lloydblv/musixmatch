package com.company.android.sample.topcharts.view

import android.annotation.TargetApi
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.drawable.AnimatedVectorDrawable
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.GridLayoutManager.SpanSizeLookup
import android.support.v7.widget.RecyclerView
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.company.android.sample.R
import com.company.android.sample.commons.BaseFragment
import com.company.android.sample.commons.getItem
import com.company.android.sample.commons.isK
import com.company.android.sample.commons.isL
import com.company.android.sample.commons.toGone
import com.company.android.sample.commons.toVisible
import com.company.android.sample.config.AppConfig
import com.company.android.sample.lyrics.view.LyricsFragment
import com.company.android.sample.topcharts.adapter.TopChartsAdapter
import com.company.android.sample.topcharts.domain.TopChartsVMFactory
import com.company.android.sample.topcharts.presenter.TopChartsViewModel
import com.company.android.sample.utils.EndlessRecyclerViewScrollListener
import com.company.android.sample.widgets.GridItemDividerDecoration
import ir.zinutech.android.domain.entities.TrackEntity
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator
import kotlinx.android.synthetic.main.fragment_topcharts_layout.stub_error
import kotlinx.android.synthetic.main.fragment_topcharts_layout.stub_no_connection
import kotlinx.android.synthetic.main.fragment_topcharts_layout.topcharts_fragment_parent
import kotlinx.android.synthetic.main.fragment_topcharts_layout.topcharts_fragment_pb
import kotlinx.android.synthetic.main.fragment_topcharts_layout.topcharts_fragment_recyclerview
import timber.log.Timber
import javax.inject.Inject

class TopChartsFragment : BaseFragment() {

  companion object {
    const val TAG = "TopChartsFragment"
    fun newInstance() = TopChartsFragment()
  }

  @Inject
  lateinit var factory: TopChartsVMFactory

  private lateinit var viewModel: TopChartsViewModel

  private var errorViewTv: TextView? = null


  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
      savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_topcharts_layout,
      container, false)

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    (activity?.application as AppConfig).createTopChartsComponent().inject(this)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    if (isL()) {
      reenterTransition = null
      allowReturnTransitionOverlap = false
    }

    viewModel = ViewModelProviders.of(this, factory).get(TopChartsViewModel::class.java)
    if (savedInstanceState == null) {
      viewModel.getTopCharts()
    }
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    viewModel.viewState.observe(this, Observer { viewState ->

      //      Timber.d("observe(), viewState:[%s],topCharts:[%s],pageTopCharts:[%s], itemCount:[%s]",
//          viewState?.isLoading, viewState?.topCharts?.size, viewState?.pageTopCharts?.size,
//          (topcharts_fragment_recyclerview.adapter as TopChartsAdapter).itemCount)
      if (viewState != null) {
        if (viewState.isLoading) {
          (topcharts_fragment_recyclerview.adapter as TopChartsAdapter).dataStartedLoading()
          topcharts_fragment_pb.toVisible()
        } else {
          (topcharts_fragment_recyclerview.adapter as TopChartsAdapter).dataFinishedLoading()
          topcharts_fragment_pb.toGone()
        }

        (topcharts_fragment_recyclerview.adapter as TopChartsAdapter).apply {
          if (itemCount == 0) {
            topcharts_fragment_recyclerview.itemAnimator = FadeInUpAnimator()
            if (viewState.topCharts?.isNotEmpty() == true) {
              addAll(viewState.topCharts!!)
            }
          } else {
            topcharts_fragment_recyclerview.itemAnimator = null

            viewState.pageTopCharts?.let {
              (topcharts_fragment_recyclerview.adapter as TopChartsAdapter).addAll(it)
              viewState.pageTopCharts = null
            }
          }
        }

      }
    })

    viewModel.errorState.observe(this, Observer { throwable ->
      throwable?.let {
        checkErrorState(it)
      }
    })
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    Timber.d("onViewCreatedX()")

    topcharts_fragment_recyclerview.apply {
      setHasFixedSize(true)
      val spanCount = resources.getInteger(R.integer.num_columns)
      val gridLayoutManager = GridLayoutManager(context, spanCount)
      addItemDecoration(GridItemDividerDecoration(context, R.dimen.divider_height,
          R.color.divider))
      layoutManager = gridLayoutManager
//      itemAnimator = FadeInUpAnimator()
      addOnScrollListener(object : EndlessRecyclerViewScrollListener(gridLayoutManager) {
        override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
          Timber.d("onLoadMore(), page:[%s], totalItemsCount:[%s]", page, totalItemsCount)
          post {
            (adapter as TopChartsAdapter).dataStartedLoading()
          }
          viewModel.getNextTopCharts()
        }
      })

      adapter = TopChartsAdapter(spanCount, Glide.with(this@TopChartsFragment)) { view, _ ->
        getItem<TrackEntity>(view)?.let {

          val coverIv = view.findViewById<View>(R.id.topcharts_item_cover_iv)
          val songNameTv = view.findViewById<View>(R.id.topcharts_item_name_tv)

          fragmentManager
              ?.beginTransaction()
              ?.addSharedElement(coverIv, ViewCompat.getTransitionName(coverIv))
              ?.addSharedElement(songNameTv, ViewCompat.getTransitionName(songNameTv))
              ?.addToBackStack(LyricsFragment.TAG)
              ?.replace(R.id.container,
                  LyricsFragment.newInstance("${it.trackId}", it.albumArtMedium,
                      it.albumArtLarge, it.artistName, it.trackName))
              ?.commit()
        }
      }

      gridLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
        override fun getSpanSize(position: Int): Int {
          return (adapter as TopChartsAdapter).getItemColumnSpan(position)
        }
      }
    }
  }

  override fun onDestroyView() {
    super.onDestroyView()
    (activity?.application as AppConfig).releaseTopChartsComponent()
  }


  private fun checkErrorState(throwable: Throwable) {

    if ((topcharts_fragment_recyclerview.adapter as TopChartsAdapter).getDataItemCount() == 0) {
      // if grid is empty check whether we're loading or if no filters are selected

      if (errorViewTv == null) {
        errorViewTv = stub_error.inflate() as TextView
        errorViewTv?.setOnClickListener {
          if (isK()) {
            TransitionManager.beginDelayedTransition(topcharts_fragment_parent)
          }
          noConnectionIv?.toGone()
          errorViewTv?.toGone()
          topcharts_fragment_pb?.toVisible()
          viewModel.refreshTopCharts()
        }
      }

      errorViewTv?.text = "${throwable.message}\n Retry?"
      errorViewTv?.toVisible()
    } else {
      Toast.makeText(activity, throwable.message, Toast.LENGTH_SHORT).show()

    }
  }

  private fun checkConnectivity() {
    if (!isL()) return
    val connectivityManager = activity?.getSystemService(
        Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    val activeNetworkInfo = connectivityManager?.activeNetworkInfo
    connected = activeNetworkInfo != null && activeNetworkInfo.isConnected
    if (!connected && (topcharts_fragment_recyclerview.adapter as TopChartsAdapter).getDataItemCount() == 0) {
      topcharts_fragment_pb.toGone()
      if (noConnectionIv == null) {
        noConnectionIv = stub_no_connection.inflate() as ImageView
      }
      val avd = activity?.getDrawable(R.drawable.avd_no_connection) as? AnimatedVectorDrawable
      if (noConnectionIv != null && avd != null) {
        noConnectionIv?.setImageDrawable(avd)
        avd.start()
      }

      connectivityManager?.registerNetworkCallback(
          NetworkRequest.Builder()
              .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build(),
          connectivityCallback)
      monitoringConnectivity = true
    }
  }

  private val connectivityCallback =
      if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
        object : ConnectivityManager.NetworkCallback() {
          override fun onAvailable(network: Network) {
            connected = true
            if ((topcharts_fragment_recyclerview.adapter as TopChartsAdapter).getDataItemCount() != 0) return
            activity?.runOnUiThread {
              TransitionManager.beginDelayedTransition(topcharts_fragment_parent)
              noConnectionIv?.toGone()
              errorViewTv?.toGone()
              topcharts_fragment_pb?.toVisible()
              viewModel.refreshTopCharts()
            }

          }

          override fun onLost(network: Network) {
            connected = false
          }
        }
      } else {
        null
      }

  override fun onResume() {
    super.onResume()
    checkConnectivity()
  }

  private var monitoringConnectivity = false
  private var connected = true
  private var noConnectionIv: ImageView? = null


  override fun onPause() {
    if (isL() && monitoringConnectivity) {
      val connectivityManager = activity?.getSystemService(
          Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
      connectivityManager?.unregisterNetworkCallback(connectivityCallback)
      monitoringConnectivity = false
    }
    super.onPause()

  }

}