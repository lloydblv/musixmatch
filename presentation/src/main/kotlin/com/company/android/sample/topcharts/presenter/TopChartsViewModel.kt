package com.company.android.sample.topcharts.presenter

import android.arch.lifecycle.MutableLiveData
import com.company.android.sample.commons.BaseViewModel
import com.company.android.sample.commons.SingleLiveEvent
import com.company.android.sample.topcharts.domain.TopChartsViewState
import ir.zinutech.android.domain.entities.TrackEntity
import ir.zinutech.android.domain.usecases.GetTopCharts
import timber.log.Timber

class TopChartsViewModel(
    private val getTopChartsUseCase: GetTopCharts) : BaseViewModel() {

  var viewState: MutableLiveData<TopChartsViewState> = MutableLiveData()
  var errorState: SingleLiveEvent<Throwable?> = SingleLiveEvent()

  private var pageIndex: MutableLiveData<Int> = MutableLiveData()

  init {
    viewState.value = TopChartsViewState()
    pageIndex.value = 1
  }

  fun getNextTopCharts() {
    pageIndex.value = pageIndex.value!! + 1
    getTopCharts()
  }

  fun refreshTopCharts() {
    pageIndex.value = 1
    getTopCharts()
  }

  fun getTopCharts() {
    addDisposable(
        getTopChartsUseCase.get(pageIndex.value!!)
            .subscribe({ topCharts ->
              viewState.value?.let {

                val newState = this.viewState.value?.copy(isLoading = false,
                    pageTopCharts = topCharts)

                val newTotalTopCharts = ArrayList<TrackEntity>()
                if (newState?.topCharts?.isNotEmpty() == true) {
                  newTotalTopCharts.addAll(newState.topCharts!!)
                }

                if (newState?.pageTopCharts?.isNotEmpty() == true) {
                  newTotalTopCharts.addAll(newState.pageTopCharts!!)
                }
                newState?.topCharts = newTotalTopCharts
                this.viewState.value = newState
                this.errorState.value = null
              }
            }, {
              Timber.e(it, "while getTopChartsUseCase()")
              viewState.value = viewState.value?.copy(isLoading = false)
              errorState.value = it
            })
    )
  }
}