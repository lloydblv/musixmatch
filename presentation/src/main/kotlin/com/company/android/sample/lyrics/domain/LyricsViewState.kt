package com.company.android.sample.lyrics.domain

import ir.zinutech.android.domain.entities.LyricsEntity

data class LyricsViewState(
    var isLoading: Boolean = true,
    var lyricsEntity: LyricsEntity? = null
)