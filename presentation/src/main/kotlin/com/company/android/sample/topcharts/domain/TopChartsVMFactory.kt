package com.company.android.sample.topcharts.domain

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.company.android.sample.topcharts.presenter.TopChartsViewModel
import ir.zinutech.android.domain.usecases.GetTopCharts

class TopChartsVMFactory(
    private val useCase: GetTopCharts
): ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    return TopChartsViewModel(useCase) as T
  }

}