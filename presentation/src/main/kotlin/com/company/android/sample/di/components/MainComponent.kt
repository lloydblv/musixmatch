package com.company.android.sample.di.components

import com.company.android.sample.lyrics.injection.LyricsModule
import com.company.android.sample.lyrics.injection.LyricsSubComponent
import com.company.android.sample.di.modules.ContextModule
import com.company.android.sample.di.modules.DataModule
import com.company.android.sample.di.modules.NetModule
import com.company.android.sample.topcharts.injection.TopChartsModule
import com.company.android.sample.topcharts.injection.TopChartsSubComponent
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [
  (ContextModule::class),
  (NetModule::class),
  (DataModule::class)])
interface MainComponent {
  fun plus(topChartsModule: TopChartsModule): TopChartsSubComponent
  fun plus(lyricsModule: LyricsModule): LyricsSubComponent
}