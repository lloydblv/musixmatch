package com.company.android.sample.topcharts.injection

import com.company.android.sample.topcharts.view.TopChartsFragment
import dagger.Subcomponent


@Subcomponent(modules = [TopChartsModule::class])
interface TopChartsSubComponent {
  fun inject(topChartsFragment: TopChartsFragment)
}