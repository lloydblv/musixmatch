package com.company.android.sample.lyrics.adapter

import android.os.Build
import android.support.v4.content.ContextCompat
import android.view.View
import com.company.android.sample.R
import com.company.android.sample.commons.BaseLceAdapter
import com.company.android.sample.commons.BaseViewHolder
import com.company.android.sample.commons.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_lyrics_layout.view.lyrics_item_name_tv

class LyricsAdapter(
    onListItemClickListener: RecyclerViewClickListener) : BaseLceAdapter<BaseViewHolder<String>, String>(
    onListItemClickListener) {


  override fun getLayout(viewType: Int): Int = R.layout.item_lyrics_layout

  override fun getViewHolder(parent: View,
      viewType: Int): BaseViewHolder<String> = ViewHolder(parent)

  class ViewHolder(view: View) : BaseViewHolder<String>(view) {
    init {
      if (Build.VERSION.SDK_INT >= 23) {
        itemView.foreground = ContextCompat.getDrawable(itemView.context, R.drawable.light_ripple)
      }
    }
    override fun bind(item: String) {
      itemView.lyrics_item_name_tv.text = item
    }
  }
}