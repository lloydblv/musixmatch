package com.company.android.sample.di.modules

import dagger.Module
import dagger.Provides
import ir.zinutech.android.data.api.Api
import ir.zinutech.android.data.repositories.MusicRepositoryImpl
import ir.zinutech.android.data.repositories.MusicDataStore
import ir.zinutech.android.domain.MusicRepository
import javax.inject.Singleton


@Module
class DataModule {
  @Singleton
  @Provides
  fun provideCarsRepository(api: Api): MusicRepository = MusicRepositoryImpl(MusicDataStore(api))
}