package com.company.android.sample.topcharts.injection

import com.company.android.sample.commons.AsyncTransformer
import com.company.android.sample.topcharts.domain.TopChartsVMFactory
import dagger.Module
import dagger.Provides
import ir.zinutech.android.domain.MusicRepository
import ir.zinutech.android.domain.usecases.GetTopCharts

@ToChartsScope
@Module
class TopChartsModule {

  @Provides
  fun provideGetTopChartsUseCase(musicRepository: MusicRepository) = GetTopCharts(
      AsyncTransformer(), musicRepository)

  @Provides
  fun provideTopChartsVMFactory(useCase: GetTopCharts) = TopChartsVMFactory(useCase)
}