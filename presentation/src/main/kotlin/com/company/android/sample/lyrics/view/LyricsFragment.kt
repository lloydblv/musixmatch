package com.company.android.sample.lyrics.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.transition.TransitionInflater
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.transition.Fade
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.company.android.sample.R
import com.company.android.sample.lyrics.adapter.LyricsAdapter
import com.company.android.sample.lyrics.domain.LyricsVMFactory
import com.company.android.sample.lyrics.presenter.LyricsViewModel
import com.company.android.sample.commons.BaseFragment
import com.company.android.sample.commons.isL
import com.company.android.sample.commons.toGone
import com.company.android.sample.commons.toVisible
import com.company.android.sample.config.AppConfig
import ir.zinutech.android.domain.usecases.GetLyrics
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator
import kotlinx.android.synthetic.main.fragment_lyrics_layout.lyrics_fragment_cover_iv
import kotlinx.android.synthetic.main.fragment_lyrics_layout.lyrics_fragment_header_tv
import kotlinx.android.synthetic.main.fragment_lyrics_layout.lyrics_fragment_pb
import kotlinx.android.synthetic.main.fragment_lyrics_layout.lyrics_fragment_recyclerview
import javax.inject.Inject

class LyricsFragment : BaseFragment() {

  companion object {
    const val TAG = "LyricsFragment"
    fun newInstance(songId: String, coverUrl: String, largeCoverUrl: String, artistName: String,
        songName: String) = LyricsFragment().apply {
      arguments = Bundle().apply {
        putString(ARG_SONG_ID, songId)
        putString(ARG_LARGE_COVER_URL, largeCoverUrl)
        putString(ARG_COVER_URL, coverUrl)
        putString(ARG_ARTIST_NAME, artistName)
        putString(ARG_SONG_NAME, songName)
      }
    }

    private const val ARG_COVER_URL = "arg_cover_url"
    private const val ARG_LARGE_COVER_URL = "arg_large_cover_url"
    private const val ARG_ARTIST_NAME = "arg_artist_name"
    private const val ARG_SONG_NAME = "arg_song_name"
    private const val ARG_SONG_ID = "arg_song_id"
  }


  @Inject
  lateinit var getLyricsUsecase: GetLyrics

  private lateinit var viewModel: LyricsViewModel


  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
      savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_lyrics_layout,
      container, false)

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    (activity?.application as AppConfig).createLyricsComponent().inject(this)
  }


  private fun loadCover(loadFromCache: Boolean = false) {
    Glide.with(this)
        .load(arguments?.getString(ARG_COVER_URL))
//        .thumbnail(Glide.with(this).load(arguments?.getString(ARG_COVER_URL))
//            .apply(RequestOptions.bitmapTransform(ColorFilterTransformation(Color.parseColor(TopChartsAdapter.COVER_OVERLAY_COLOR)))))
//        )
        .apply(
            RequestOptions()
                .dontTransform()
                .onlyRetrieveFromCache(true)
        )
        .listener(object : RequestListener<Drawable> {
          override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?,
              isFirstResource: Boolean): Boolean {
            startPostponedEnterTransition()
            return false
          }

          override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?,
              dataSource: DataSource?, isFirstResource: Boolean): Boolean {
            startPostponedEnterTransition()
            return false
          }
        })
        .into(lyrics_fragment_cover_iv)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)


    postponeEnterTransition()
    if (true) {
      enterTransition = android.support.transition.Fade().apply {
        if (isL()) {
          excludeTarget(android.R.id.statusBarBackground, true)
          excludeTarget(android.R.id.navigationBarBackground, true)
        }
        excludeTarget(R.id.action_bar_container, true)
      }

      sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.fragment_move_transition)
//      sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    viewModel = ViewModelProviders.of(this,
        LyricsVMFactory(getLyricsUsecase, arguments?.getString(
            ARG_SONG_ID) ?: throw IllegalArgumentException())).get(
        LyricsViewModel::class.java)
    if (savedInstanceState == null) {
      viewModel.getLyrics()
    }
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    viewModel.viewState.observe(this, Observer { viewState ->
      if (viewState != null) {
        if (viewState.isLoading) {
          lyrics_fragment_pb.toVisible()
        } else {
          lyrics_fragment_pb.toGone()
        }
        (lyrics_fragment_recyclerview.adapter as LyricsAdapter).apply {
          if (itemCount == 0) {
            if (viewState.lyricsEntity?.lyricsList?.isNotEmpty() == true) {
              addAll(viewState.lyricsEntity!!.lyricsList)
            }
          }
        }

      }
    })

    viewModel.errorState.observe(this, Observer { throwable ->
      throwable?.let {
        Toast.makeText(activity, throwable.message, Toast.LENGTH_SHORT).show()
      }
    })
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    if (isL()) {




//      sharedElementEnterTransition = TransitionSet()
//          .addTransition(ChangeImageTransform())
//          .addTransition(ChangeBounds())
//          .apply {
//            doOnEnd { loadCover() }
//          }

//
//      reenterTransition = null
//      allowReturnTransitionOverlap = false
    }
    ViewCompat.setTransitionName(lyrics_fragment_cover_iv, arguments?.getString(ARG_COVER_URL))
    ViewCompat.setTransitionName(lyrics_fragment_header_tv,
        arguments?.getString(ARG_SONG_NAME))
    loadCover(true)

    lyrics_fragment_header_tv.text = arguments?.getString(ARG_SONG_NAME)

    lyrics_fragment_recyclerview.apply {
      setHasFixedSize(true)
      val spanCount = resources.getInteger(R.integer.num_columns)
      val linearLayoutManager = LinearLayoutManager(context)
//      addItemDecoration(GridItemDividerDecoration(context, R.dimen.divider_height,
//          R.color.divider))
      layoutManager = linearLayoutManager
      itemAnimator = FadeInUpAnimator()
      adapter = LyricsAdapter() { _, _ ->
      }
    }
  }

  override fun onDestroyView() {
    super.onDestroyView()
    (activity?.application as AppConfig).releaseLyricsComponent()
  }
}