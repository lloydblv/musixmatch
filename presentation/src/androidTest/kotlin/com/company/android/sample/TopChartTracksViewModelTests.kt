package com.company.android.sample

import android.arch.lifecycle.Observer
import android.support.test.runner.AndroidJUnit4
import android.test.UiThreadTest
import com.company.android.sample.topcharts.domain.TopChartsViewState
import com.company.android.sample.topcharts.presenter.TopChartsViewModel
import io.reactivex.Single
import ir.zinutech.android.domain.MusicRepository
import ir.zinutech.android.domain.common.DomainTestUtils
import ir.zinutech.android.domain.common.TestTransformer
import ir.zinutech.android.domain.usecases.GetTopCharts
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyZeroInteractions


@Suppress("UNCHECKED_CAST")
@RunWith(AndroidJUnit4::class)
class TopChartTracksViewModelTests {

  private lateinit var topChartsViewModel: TopChartsViewModel
  private lateinit var musicRepository: MusicRepository

  private lateinit var viewObserver: Observer<TopChartsViewState>
  private lateinit var errorObserver: Observer<Throwable?>

  @Before
  @UiThreadTest
  fun before(){
    musicRepository = mock(MusicRepository::class.java)
    val getTopCharts = GetTopCharts(TestTransformer(), musicRepository)
    topChartsViewModel = TopChartsViewModel(getTopCharts)

    viewObserver = mock(Observer::class.java) as Observer<TopChartsViewState>
    errorObserver = mock(Observer::class.java) as Observer<Throwable?>

    topChartsViewModel.viewState.observeForever(viewObserver)
    topChartsViewModel.errorState.observeForever(errorObserver)
  }

  @Test
  @UiThreadTest
  fun testInitialViewStateShowsLoading(){
    verify(viewObserver).onChanged(TopChartsViewState(isLoading = true, topCharts = null, pageTopCharts = null))
    verifyZeroInteractions(viewObserver)
  }


  @Test
  @UiThreadTest
  fun testShowingTopChartsAsExpectedAndStopsLoading(){

    val trackListEntities = DomainTestUtils.generateTrackEntityList()

    `when`(musicRepository.getTracksList(1)).thenReturn(Single.just(trackListEntities))

    topChartsViewModel.getTopCharts()

    verify(viewObserver).onChanged(TopChartsViewState(isLoading = false, pageTopCharts = trackListEntities, topCharts = trackListEntities))
    verify(errorObserver).onChanged(null)
  }


  @Test
  @UiThreadTest
  fun testShowingErrorMessageWhenNeeded(){
    val throwable = Throwable("ERROR!")
    `when`(musicRepository.getTracksList(1)).thenReturn(Single.error(throwable))
    topChartsViewModel.getTopCharts()

    verify(viewObserver).onChanged(TopChartsViewState(isLoading = false, pageTopCharts = null))
    verify(errorObserver).onChanged(throwable)
  }

}