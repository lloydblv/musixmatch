package com.company.android.sample

import android.arch.lifecycle.Observer
import android.support.test.runner.AndroidJUnit4
import android.test.UiThreadTest
import com.company.android.sample.lyrics.domain.LyricsViewState
import com.company.android.sample.lyrics.presenter.LyricsViewModel
import com.company.android.sample.topcharts.domain.TopChartsViewState
import com.company.android.sample.topcharts.presenter.TopChartsViewModel
import io.reactivex.Single
import ir.zinutech.android.domain.MusicRepository
import ir.zinutech.android.domain.common.DomainTestUtils
import ir.zinutech.android.domain.common.TestTransformer
import ir.zinutech.android.domain.usecases.GetLyrics
import ir.zinutech.android.domain.usecases.GetTopCharts
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito


@Suppress("UNCHECKED_CAST")
@RunWith(AndroidJUnit4::class)
class LyricsViewModelTests {

  private lateinit var lyricsViewModel: LyricsViewModel
  private lateinit var musicRepository: MusicRepository

  private lateinit var viewObserver: Observer<LyricsViewState>
  private lateinit var errorObserver: Observer<Throwable?>

  @Before
  @UiThreadTest
  fun before() {
    musicRepository = Mockito.mock(MusicRepository::class.java)
    val getLyrics = GetLyrics(TestTransformer(), musicRepository)
    lyricsViewModel = LyricsViewModel(getLyrics, "111")

    viewObserver = Mockito.mock(Observer::class.java) as Observer<LyricsViewState>
    errorObserver = Mockito.mock(Observer::class.java) as Observer<Throwable?>

    lyricsViewModel.viewState.observeForever(viewObserver)
    lyricsViewModel.errorState.observeForever(errorObserver)
  }


  @Test
  @UiThreadTest
  fun testInitialViewStateShowsLoading(){
    Mockito.verify(viewObserver).onChanged(LyricsViewState(isLoading = true))
    Mockito.verifyZeroInteractions(viewObserver)
  }


  @Test
  @UiThreadTest
  fun testShowingLyricsAsExpectedAndStopsLoading(){

    val lyricsEntity = DomainTestUtils.generateLyricsEntity()

    Mockito.`when`(musicRepository.getLyrics("111")).thenReturn(Single.just(lyricsEntity))

    lyricsViewModel.getLyrics()

    Mockito.verify(viewObserver).onChanged(LyricsViewState(isLoading = false, lyricsEntity = lyricsEntity.value))
    Mockito.verify(errorObserver).onChanged(null)
  }


  @Test
  @UiThreadTest
  fun testShowingErrorMessageWhenNeeded(){
    val throwable = Throwable("ERROR!")
    Mockito.`when`(musicRepository.getLyrics("111")).thenReturn(Single.error(throwable))
    lyricsViewModel.getLyrics()

    Mockito.verify(viewObserver).onChanged(LyricsViewState(isLoading = false))
    Mockito.verify(errorObserver).onChanged(throwable)
  }
}