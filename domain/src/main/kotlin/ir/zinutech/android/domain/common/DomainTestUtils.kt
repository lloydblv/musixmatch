package ir.zinutech.android.domain.common

import ir.zinutech.android.domain.entities.LyricsEntity
import ir.zinutech.android.domain.entities.Optional
import ir.zinutech.android.domain.entities.TrackEntity


object DomainTestUtils {
  fun getTestTrackEntity(id: Long): TrackEntity =
      TrackEntity(id, "trackName$id", "albumName$id", "artistName$id",
          "albumArtSmall$id",
          "albumArtNormal$id", "albumArtMedium$id", "albumArtLarge$id")


  fun generateTrackEntityList(): List<TrackEntity> =
      (0..5).map { getTestTrackEntity(it.toLong()) }

  fun generateLyricsEntity(): Optional<LyricsEntity>{
    return Optional.of(LyricsEntity(generateLyricsList()))
  }

  fun generateLyricsList(): List<String> = (0..5).map { "LyricsBody_$it" }
}