package ir.zinutech.android.domain.entities

data class TrackEntity(
    val trackId: Long,
    val trackName: String,
    val albumName: String,
    val artistName: String,

    val albumArtSmall: String,
    val albumArtNormal: String,
    val albumArtMedium: String,
    val albumArtLarge: String)