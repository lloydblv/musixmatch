package ir.zinutech.android.domain

import io.reactivex.Single
import ir.zinutech.android.domain.entities.LyricsEntity
import ir.zinutech.android.domain.entities.Optional
import ir.zinutech.android.domain.entities.TrackEntity

interface MusicRepository {
  fun getTracksList(pageIndex: Int = 1): Single<List<TrackEntity>>
  fun getLyrics(trackId: String): Single<Optional<LyricsEntity>>
}