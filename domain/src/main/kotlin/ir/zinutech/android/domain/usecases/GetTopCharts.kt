package ir.zinutech.android.domain.usecases

import io.reactivex.Single
import ir.zinutech.android.domain.MusicRepository
import ir.zinutech.android.domain.common.Transformer
import ir.zinutech.android.domain.entities.TrackEntity

class GetTopCharts(transformer: Transformer<List<TrackEntity>>,
    private val musicRepository: MusicRepository) : UseCase<List<TrackEntity>>(transformer) {

  companion object {
    private const val PARAM_PAGE_INDEX = "param:page_index"
  }

  fun get(pageIndex: Int): Single<List<TrackEntity>> = single(
      hashMapOf(PARAM_PAGE_INDEX to pageIndex))

  override fun createSingle(data: Map<String, Any>?): Single<List<TrackEntity>> = data?.get(
      PARAM_PAGE_INDEX)?.let { pageIndex ->
    musicRepository.getTracksList(pageIndex as Int)
  } ?: Single.just(emptyList())
}