package ir.zinutech.android.domain

import io.reactivex.Single
import ir.zinutech.android.domain.entities.LyricsEntity
import ir.zinutech.android.domain.entities.Optional
import ir.zinutech.android.domain.entities.TrackEntity

interface MusicDataStore {
  fun getTracksList(pageIndex: Int = 0): Single<List<TrackEntity>>
  fun getLyricsByTrackId(trackId: String): Single<Optional<LyricsEntity>>
}