package ir.zinutech.android.domain.entities

data class LyricsEntity(val lyricsList: List<String>)