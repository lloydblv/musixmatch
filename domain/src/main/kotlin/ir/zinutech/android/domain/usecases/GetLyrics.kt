package ir.zinutech.android.domain.usecases

import io.reactivex.Single
import ir.zinutech.android.domain.MusicRepository
import ir.zinutech.android.domain.common.Transformer
import ir.zinutech.android.domain.entities.LyricsEntity
import ir.zinutech.android.domain.entities.Optional

class GetLyrics(transformer: Transformer<Optional<LyricsEntity>>,
    private val musicRepository: MusicRepository) : UseCase<Optional<LyricsEntity>>(transformer) {

  companion object {
    private const val PARAM_TRACK_ID = "param:track_id"
  }

  fun getByTrackId(trackId: String): Single<Optional<LyricsEntity>> = single(
      hashMapOf(PARAM_TRACK_ID to trackId))

  override fun createSingle(data: Map<String, Any>?): Single<Optional<LyricsEntity>> = data?.get(
      PARAM_TRACK_ID)?.let { trackId ->
    musicRepository.getLyrics(trackId as String)
  } ?: Single.just(Optional.empty())

}