package ir.zinutech.android.domain

import io.reactivex.Single
import ir.zinutech.android.domain.common.DomainTestUtils
import ir.zinutech.android.domain.common.TestTransformer
import ir.zinutech.android.domain.usecases.GetLyrics
import ir.zinutech.android.domain.usecases.GetTopCharts
import org.junit.Test
import org.mockito.Mockito

class UseCasesTest {
  @Test
  fun getTopTracksList() {

    val musicRepository = Mockito.mock(MusicRepository::class.java)

    Mockito.`when`(musicRepository.getTracksList(1)).thenReturn(
        Single.just(DomainTestUtils.generateTrackEntityList()))

    val getTopCharts = GetTopCharts(TestTransformer(), musicRepository)
    getTopCharts.get(1).test()
        .assertValue { it.size == 6 }
        .assertComplete()
  }

  @Test
  fun getTopTracksNoResultsReturnsEmpty() {
    val musicRepository = Mockito.mock(MusicRepository::class.java)

    Mockito.`when`(musicRepository.getTracksList()).thenReturn(Single.just(emptyList()))

    val getTopCharts = GetTopCharts(TestTransformer(), musicRepository)
    getTopCharts.get(0).test()
        .assertValue { it.isEmpty() }
        .assertComplete()
  }

  @Test
  fun getLyricsByTrack() {
    val testTrackEntity = DomainTestUtils.getTestTrackEntity(130)
    val musicRepository = Mockito.mock(MusicRepository::class.java)

    val getLyrics = GetLyrics(TestTransformer(), musicRepository)

    Mockito.`when`(musicRepository.getLyrics("${testTrackEntity.trackId}")).thenReturn(Single.just(DomainTestUtils.generateLyricsEntity()))

    getLyrics.getByTrackId("${testTrackEntity.trackId}").test()
        .assertValue { it.value?.lyricsList?.size == 6 }
        .assertComplete()

  }
}